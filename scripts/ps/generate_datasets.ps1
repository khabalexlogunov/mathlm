$env:HYDRA_FULL_ERROR = 1


# Prevent generation of 3-digit complecity samples from stucking in while-loop
python -m mathlm.core.create_dataset `
pipeline.complexity=3 `
pipeline.num_type=dec `
pipeline_run.n_records=10000 `
pipeline_run.train_size=0.8

# Same for 4-digit complexity
python -m mathlm.core.create_dataset `
pipeline.complexity=4 `
pipeline.num_type=dec `
pipeline_run.n_records=80000 `
pipeline_run.train_size=0.625

# Then generate other complexities with other n_records
python -m mathlm.core.create_dataset --multirun `
pipeline.complexity=5,6,7,8,9,10,11,12,13,14 `
pipeline.num_type=dec `
pipeline_run.n_records=100000 `
pipeline_run.train_size=0.98