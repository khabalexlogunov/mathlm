$env:HYDRA_FULL_ERROR = 1
$env:CUDA_VISIBLE_DEVICES = $1

### Run ###

# With ft adapters
# python -m mathlm.core.test system="starcoder_1b" pipeline.dataset_size=0.25
python -m mathlm.core.test system="starcoder_1b" system.peft=null pipeline.dataset_size=0.25