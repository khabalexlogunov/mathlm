export HYDRA_FULL_ERROR=1


# Prevent generation of 3-digit complecity samples from stucking in while-loop
python3 -m mathlm.core.create_dataset \
pipeline.complexity=3 \
pipeline_run.n_records=4000 \
pipeline_run.train_size=0.9

# Then generate other complexities with other n_records
python3 -m mathlm.core.create_dataset --multirun \
pipeline.complexity=4,5,6,7,8,9,10,11,12 \
pipeline_run.n_records=10000 \
pipeline_run.train_size=0.96