### Env ###

export CUDA_VISIBLE_DEVICES=$1

### Run ###

# StarCoder (tuned and basic)
python3 -m mathlm.core.test system="starcoder_1b" pipeline.dataset_size=0.25
python3 -m mathlm.core.test system="starcoder_1b" system.peft=null pipeline.dataset_size=0.25

# Phi-1.5
python3 -m mathlm.core.test system="phi-1_5" pipeline.dataset_size=0.25

# Pythia
# python3 -m mathlm.core.test system="pythia" pipeline.dataset_size=0.25