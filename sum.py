import warnings
import argparse
from typing import Iterable, Union, Tuple, List
from decimal import Decimal

from hydra import compose, initialize

from mathlm.utils.task import HFTaskWithPretrainedPEFT
from mathlm.utils.model_io import PROMPTS, postprocess_model_output


warnings.filterwarnings("ignore")


DecOrDecs = Union[Decimal, Iterable[Decimal]]


class AdditionPipeline(HFTaskWithPretrainedPEFT):
    def _process_sample(self, term1: float, term2: float) -> Decimal:
        is_phi = "phi-1_5" in self.config.system.model.pretrained_model_name_or_path
        term1 = Decimal(term1)
        term2 = Decimal(term2)

        if is_phi:
            input = f"{term1},{term2},"
        else:
            input = PROMPTS[self.config.system.prompt_template]["test"].format(term1, term2)

        text = self.infer_sample(input, max_length=64)
        pred = postprocess_model_output(text, self.config.system.prompt_template, is_phi)

        return Decimal(pred)

    def run(self, term1: DecOrDecs, term2: DecOrDecs) -> Tuple[DecOrDecs, List[str]]:
        assert type(term1) == type(term2)
        if isinstance(term1, Iterable):
            assert len(term1) == len(term2)

        self._init_model()
        self._init_tokenizer()
        self._init_peft()

        self.model.to(self.config.device)

        if isinstance(term1, Iterable):
            preds, lines = [], []
            for t1, t2 in zip(term1, term2):
                preds.append(self._process_sample(t1, t2))
                lines.append(f"{t1} + {t2} = {preds[-1]}")

        else:
            preds = [self._process_sample(term1, term2)]
            lines = [f"{term1} + {term2} = {preds[-1]}"]

        return preds, lines


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--term1",
        required=True,
        type=str,
        help="Decimal or comma-separated decimals.",
    )
    parser.add_argument(
        "--term2",
        required=True,
        type=str,
        help="Decimal or comma-separated decimals.",
    )
    parser.add_argument(
        "--system",
        type=str,
        default="starcoder-1b",
        choices=["starcoder-1b", "phi-1_5", "pythia-1b"],
    )
    parser.add_argument(
        "--use-ft",
        action="store_true",
    )

    args = parser.parse_args()
    return args


def main(args: argparse.Namespace):
    # Read config
    with initialize("./configs"):
        overrides = [f"system={args.system}"]
        if not args.use_ft:
            overrides += [f"system.peft=null"]
        cfg = compose("test.yaml", overrides)

    # Preprocess input
    term1 = args.term1.split(",")
    term2 = args.term2.split(",")

    # Init and run pipe
    pipe = AdditionPipeline(cfg)
    _, lines = pipe.run(term1, term2)

    # Show
    print("Output:")
    for l in lines:
        print(l)


if __name__ == "__main__":
    args = parse_args()
    main(args)
