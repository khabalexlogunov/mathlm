# TODO:
# (v) 1. Написать основной трейн (https://huggingface.co/transformers/v3.2.0/custom_datasets.html)
#   1.1. Написать сплит трейна на трейн и эвал.
# 2. Написать замеры качества
# 3. Написать улучшенный трейн:
#   (v) 3.1. FT через адаптеры
#   3.2. Curriclulum training - повышение сложности обучения по мере приблежения к концу.

import warnings
from typing import Dict

import hydra
import torch
from omegaconf import DictConfig
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LRScheduler
from torch.utils.data import DataLoader, Dataset, ConcatDataset
from transformers import (
    Trainer,
    BitsAndBytesConfig,
    DataCollatorForLanguageModeling,
)
from transformers.trainer_callback import PrinterCallback
from peft import get_peft_model

from mathlm.utils.task import HFTask


warnings.filterwarnings("ignore")


class TrainTask(HFTask):
    optimizer: Optimizer
    lr_scheduler: LRScheduler
    trainer: Trainer
    datasets: Dict[str, Dataset] = {}
    loaders: Dict[str, DataLoader] = {}

    def _init_model(self):
        bnb_cfg = BitsAndBytesConfig(
            load_in_4bit=True,
            bnb_4bit_use_double_quant=True,
            bnb_4bit_quant_type="nf4",
            bnb_4bit_compute_dtype=torch.float16,
        )
        super()._init_model(quantization_config=bnb_cfg)

    def _init_tokenizer(self):
        super()._init_tokenizer()
        self.tokenizer.pad_token = self.tokenizer.eos_token

    def _apply_peft(self):
        if self.config.system.peft is not None:
            print("Applting adapters weights.")
            peft_cfg = hydra.utils.instantiate(self.config.system.peft, _convert_="all")
            self.model = get_peft_model(self.model, peft_cfg)

    def _read_dataset(self, part: str):
        print(f"Loading {part} data.")
        datasets = []
        for base_id, base_cfg in self.config.bases[part].items():
            dset = hydra.utils.instantiate(
                base_cfg,
                tokenizer=self.tokenizer,
                prompt_template=self.config.system.prompt_template,
                size=self.config.pipeline.dataset_size,
            )
            datasets.append(dset)

        return ConcatDataset(datasets)

    def _init_datasets(self):
        self.datasets["train"] = self._read_dataset("train")
        self.datasets["test"] = self._read_dataset("test")

    def _init_trainer(self):
        print("Instantiating trainer.")
        args = hydra.utils.instantiate(self.config.system.trainer)
        self.trainer = Trainer(
            model=self.model,
            args=args,
            train_dataset=self.datasets["train"],
            eval_dataset=self.datasets["test"],
            data_collator=DataCollatorForLanguageModeling(self.tokenizer, mlm=False),
        )

    def _prepare_training(self):
        self.trainer.remove_callback(PrinterCallback)

    def run(self):
        self._init_tokenizer()
        self._init_datasets()

        self._init_model()
        self._apply_peft()
        self._init_trainer()

        print("Run...")
        self._prepare_training()
        self.trainer.train()
        self.trainer.push_to_hub()


@hydra.main("../../configs", "finetune", version_base="1.3")
def main(cfg: DictConfig):
    task = TrainTask(cfg)
    task.run()


if __name__ == "__main__":
    main()
