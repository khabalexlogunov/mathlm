import json
from typing import List, Union, Iterable, Dict
from pathlib import Path
from decimal import Decimal

import tqdm
import hydra
import pandas as pd
from omegaconf import DictConfig

from mathlm.utils.task import HFTaskWithPretrainedPEFT
from mathlm.utils.metrics import Metric
from mathlm.utils.model_io import PROMPTS, postprocess_model_output


class TestTask(HFTaskWithPretrainedPEFT):
    metrics: Dict[str, Metric]
    outputs_dir: Path
    metrics_dir: Path

    def _init_tokenizer(self):
        super()._init_tokenizer()
        self.tokenizer.pad_token = self.tokenizer.eos_token

    def _init_metrics(self):
        self.metrics = {}
        for m_name, m_cfg in self.config.pipeline.metrics.items():
            self.metrics[m_name] = hydra.utils.instantiate(m_cfg)

    def _infer_dataset(self, dataset_path: Union[str, Path]) -> List[str]:
        is_phi = "phi-1_5" in self.config.system.model.pretrained_model_name_or_path
        prompt_template = self.config.system.prompt_template

        with open(dataset_path.path, "r") as file:
            lines = file.readlines()
            triplets = [line.strip().split(",") for line in lines]

        # Apply size restriction
        triplets = triplets[: int(len(triplets) * self.config.pipeline.dataset_size)]
        print(f"Evaluating on {dataset_path.path} ({len(triplets)} samples).")

        preds, trues = [], []
        for triplet in tqdm.tqdm(triplets):
            # Surround inputs with prompt.
            # phi-1.5 model was finetuned on wrong prompt. So this is a plug for that case.
            if not is_phi:
                input = PROMPTS[prompt_template]["test"].format(triplet[0], triplet[1])
            else:
                input = ",".join(triplet[:2]) + ","

            # Generate
            text = self.infer_sample(input)

            # Postprocess and accumulate
            try:
                pred = postprocess_model_output(text, prompt_template, is_phi)
            except:
                print("Error during processing model's output. Skipping")
                continue

            trues.append(Decimal(triplet[-1]))
            preds.append(pred)

        return preds, trues

    def _compute_metrics(self, y_pred: Iterable[float], y_true: Iterable[float]):
        metrics = {}
        for m_name, metric in self.metrics.items():
            metrics[m_name] = metric.compute(y_pred, y_true)

        return metrics

    def _create_output_dir_tree(self, output_dir: Union[str, Path]):
        model_name = self.config.system.model.pretrained_model_name_or_path.split("/")[-1]
        ft_suffix = "-ft" if self.config.system.get("peft", None) is not None else ""
        model_dir = Path(output_dir) / (model_name + ft_suffix)
        self.outputs_dir = model_dir / "outputs"
        self.metrics_dir = model_dir / "metrics"
        self.outputs_dir.mkdir(parents=True, exist_ok=True)
        self.metrics_dir.mkdir(parents=True, exist_ok=True)

    def run(self):
        self._init_tokenizer()
        self._init_model()
        self._init_peft()
        self.model.to(self.config.device)

        self._init_metrics()

        # Run
        output_dir = Path(self.config.output_dir)
        self._create_output_dir_tree(output_dir)
        for base, base_cfg in self.config.bases["test"].items():
            preds, trues = self._infer_dataset(base_cfg)

            # Log outputs
            df = pd.DataFrame()
            df["pred"] = preds
            df["true"] = trues
            df.to_csv(self.outputs_dir / f"{base}.csv", index=False)

            # Compute and log metrics
            metrics = self._compute_metrics(trues, preds)
            with open(self.metrics_dir / f"{base}.json", "w") as file:
                json.dump(metrics, file, indent="\t")


@hydra.main("../../configs", "test", version_base="1.3")
def main(cfg: DictConfig):
    task = TestTask(cfg)
    task.run()


if __name__ == "__main__":
    main()
