import random
import shutil
from typing import List, Tuple, Literal, Union
from pathlib import Path
from numbers import Number
from decimal import Decimal

import tqdm
import hydra
from omegaconf import DictConfig

from mathlm.utils.basic import cast


def _generate_random_int(length: int) -> int:
    """Generates random interger of specified length."""

    start = int("1" + "0" * (length - 1))
    end = int("9" * length)

    return random.randint(start, end)


def _generate_random_decimal(length: int) -> Decimal:
    """Generates random float of specified length."""

    int_str = str(_generate_random_int(length))
    point_id = random.randint(1, length - 1)
    float_str = int_str[:point_id] + "." + int_str[point_id:]

    return Decimal(float_str)


def generate_triplet(
    complexity: int,
    num_type: Literal["int", "dec"] = "int",
) -> List[Tuple[Number, Number, Number]]:
    """Generates triplet of two numbers and their sum.

    Args:
        complexity (int): Length (count of symbols) of resulting number.

    Returns:
        List[Tuple[Number, Number, Number]]: Triplet in the of form [first_term, second_term, sum]
    """

    if num_type == "int":
        target = _generate_random_int(complexity)
        num1 = random.randint(0, target)
        num2 = target - num1

    elif num_type == "dec":
        target = _generate_random_decimal(complexity)
        num1 = round(random.random() * float(target), random.randint(1, complexity))
        num1 = Decimal(str(num1))
        num2 = target - num1

    return (num1, num2, target)


class DatasetGenerator:
    def __init__(
        self, 
        output_dir: Union[str, Path], 
        complexity: int = 3,
        num_type: Literal["int", "dec"] = "int"
    ):

        self.output_dir = cast(output_dir, Path)
        self.complexity = complexity
        self.num_type = num_type

        self._common_part_path: Path = None

    def _prepare_base_tree(self):
        parts = ["temp", "test", "train"]
        for p in parts:
            (self.output_dir / p).mkdir(parents=True, exist_ok=True)

    def _generate_common_base(self, n_records: int):
        """Generate base with sum operations.

        Args:
            n_records (int): Count of records in resulting dataset.
        """
        
        self._common_part_path = self.output_dir / "temp" / f"{self.num_type}_{self.complexity}.txt"

        triplets = set()
        for _ in tqdm.tqdm(range(n_records)):
            triplet = generate_triplet(self.complexity, self.num_type)
            while triplet in triplets:
                triplet = generate_triplet(self.complexity, self.num_type)

            triplets.add(triplet)

        # lines = [f"{tr[0]}+{tr[1]}={tr[2]}\n" for tr in triplets]
        lines = [",".join(map(str, tr)) + "\n" for tr in triplets]
        with open(self._common_part_path, "w") as file:
            file.writelines(lines)

    def _split(self, ratio: float):
        """Splits generated common base into train and test.

        Args:
            ratio (float): Size of train part.
        """

        file_name = self._common_part_path.name

        txt_path = cast(self._common_part_path, Path)
        with open(txt_path, "r") as file:
            lines = file.readlines()

        part1_size = int(len(lines) * ratio)
        random.shuffle(lines)
        part1, part2 = lines[:part1_size], lines[part1_size:]

        with open(self.output_dir / "train" / file_name, "w") as file:
            file.writelines(part1)

        with open(self.output_dir / "test" / file_name, "w") as file:
            file.writelines(part2)

    def generate(self, n_records: int, train_size: float):

        assert 0.0 < train_size < 1.0, "train_size should be between 0.0 and 1.0"

        self._prepare_base_tree()
        self._generate_common_base(n_records)
        self._split(train_size)
        
        # Remove temp dir
        shutil.rmtree(self.output_dir / "temp", ignore_errors=True)


@hydra.main(config_path="../../configs", config_name="create_dataset")
def main(cfg: DictConfig):
    generator = hydra.utils.instantiate(cfg.pipeline)
    generator.generate(**cfg.pipeline_run)


if __name__ == "__main__":
    main()
