import sys
from typing import Any, Type


def cast(val: Any, type_: Type) -> Type:
    if not isinstance(val, type_):
        try:
            val = type_(val)
        except TypeError:
            print(f"Can't cast value {val} to type {type_}")
            sys.exit()

    return val
