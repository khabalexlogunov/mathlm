from abc import ABC, abstractmethod

import torch
from omegaconf import DictConfig
from peft import PeftModel
from transformers import AutoTokenizer, AutoModelForCausalLM, BasicTokenizer


class Task(ABC):
    @abstractmethod
    def run(self):
        raise NotImplementedError()


class TaskWithConfig(Task, ABC):
    def __init__(self, config: DictConfig):
        self.config = config


class HFTask(TaskWithConfig, ABC):
    model: torch.nn.Module
    tokenizer: BasicTokenizer

    def _init_model(self, *args, **kwargs):
        print("Instantiating model.")
        kwargs.update(**self.config.system.model)
        self.model = AutoModelForCausalLM.from_pretrained(*args, **kwargs)

    def _init_tokenizer(self, *args, **kwargs):
        print("Instantiating tokenizer.")
        kwargs.update(**self.config.system.tokenizer)
        self.tokenizer = AutoTokenizer.from_pretrained(*args, **kwargs)

    def infer_sample(self, sample: str, max_length: int = 96) -> str:
        # Convert input to tokens
        tokens = self.tokenizer(
            sample,
            return_tensors="pt",
            return_attention_mask=False,
        )
        tokens = tokens.to(self.config.device)

        # Generate continuation and decode to text
        output = self.model.generate(**tokens, max_length=max_length)
        generated_text = self.tokenizer.batch_decode(output)[0]

        return generated_text


class HFTaskWithPretrainedPEFT(HFTask):
    def _init_peft(self):
        if self.config.system.get("peft", None) is not None:
            print("Merging with adapters weights.")
            model_id = self.config.system.trainer.output_dir.split("/")[-1]
            model_id = f"Wimplex/{model_id}"
            peft_model = PeftModel.from_pretrained(self.model, model_id)
            self.model = peft_model.merge_and_unload()
        else:
            print("Adapters has not specified. Using original model for inference.")
