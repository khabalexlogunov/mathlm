from typing import Union
from pathlib import Path

from torch.utils.data import Dataset
from transformers import BasicTokenizer

from mathlm.utils.model_io import PROMPTS


class MathDataset(Dataset):
    def __init__(
        self,
        name: str,
        path: Union[str, Path],
        complexity: int,
        tokenizer: BasicTokenizer,
        prompt_template: str,
        size: float = 1.0,
    ):
        self._name = name
        self._complexity = complexity
        self._tokenizer = tokenizer

        self._template = PROMPTS[prompt_template]["train"]
        with open(path, "r") as file:
            lines = file.readlines()
            self._lines = lines[: int(len(lines) * size)]

    def __getitem__(self, index: int):
        line = self._lines[index]
        line = self._template.format(*line.split(",")) # Fixed
        tokens = self._tokenizer(line, truncation=True, padding=True, max_length=256)
        return tokens

    def __len__(self) -> int:
        return len(self._lines)
