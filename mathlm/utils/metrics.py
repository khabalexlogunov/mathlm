from abc import ABC, abstractmethod
from typing import Iterable, Tuple
from decimal import Decimal

import numpy as np


__all__ = [
    "RelativeError",
    "ThresholdedAccuracy",
    "MeanAbsolutePercentageError",
]


class Metric(ABC):
    @abstractmethod
    def compute(self, y_pred: Iterable[Decimal], y_true: Iterable[Decimal]):
        raise NotImplementedError()


class MeanAbsolutePercentageError(Metric):
    def __init__(self, eps: float = 1e-12):
        self.eps = eps

    def compute(self, y_pred: Iterable[Decimal], y_true: Iterable[Decimal]):
        y_pred = np.array(y_pred, dtype=np.float64)
        y_true = np.array(y_true, dtype=np.float64)
        metric = abs((y_true - y_pred) / np.maximum(self.eps, y_true))
        metric = metric.sum() / len(y_pred)

        return metric


class ThresholdedAccuracy(Metric):
    def __init__(self, threshold: float = 0.0):
        self.threshold = threshold

    def compute(self, y_pred: Iterable[Decimal], y_true: Iterable[Decimal]) -> float:
        correct_count = sum(1 if abs(t - p) <= self.threshold else 0 for t, p in zip(y_true, y_pred))
        metric = correct_count / len(y_pred)

        return metric


class RelativeError(Metric):
    def __init__(self, threshold: float = 0.0, eps: float = 1e-12):
        assert 0.0 <= threshold <= 1.0
        self.threshold = threshold
        self.eps = Decimal(eps)

    def compute(self, y_pred: Iterable[Decimal], y_true: Iterable[Decimal]):
        def mape(pred_true: Tuple[float]):
            pred, true = pred_true
            return abs((true - pred) / max(true, self.eps))

        mapes = list(map(mape, zip(y_pred, y_true)))
        mapes = np.array(mapes)
        metric = (mapes <= self.threshold).astype(np.uint8).sum() / mapes.shape[0]

        return metric
