from decimal import Decimal


PROMPTS = {
    # Simple addition operation
    "sum": {"train": """{} + {} = {},\n""", "test": """{} + {} = """},
    # Addition through python function. Useful for codegen models.
    "python_sum": {
        "train": """def sum(a: float, b: float):\n\tprint(a + b)\n\nsum({}, {}) # Output: {}""",
        "test": """def sum(a: float, b: float):\n\tprint(a + b)\n\nsum({}, {}) # Output: """,
    },
}


def _postprocess_for_buggy_phi(text: str) -> Decimal:
    return Decimal(text.split("\n")[0].split(",")[2])


def _postprocess_addition_model_output(text: str) -> Decimal:
    return Decimal(text.split(",\n")[0].split("=")[-1])


def _postprocess_python_addition_model_output(text: str) -> Decimal:
    text = " ".join(text.split("# Output: ")[1:])
    parts = text.strip().split()

    # Detect if output is in form of: "Output: num1 + num2 = ans"
    if len(parts) >= 5 and parts[1] == "+" and parts[3] == "=":
        ans = parts[4]
    else:
        ans = parts[0]

    return Decimal(ans)


def postprocess_model_output(text: str, prompt_template: str, is_phi: bool = False) -> Decimal:
    if is_phi:
        # Oops, forgot to use sum prompt template during phi-1.5 finetuning
        res = _postprocess_for_buggy_phi(text)
    else:
        if prompt_template == "sum":
            res = _postprocess_addition_model_output(text)
        elif prompt_template == "python_sum":
            res = _postprocess_python_addition_model_output(text)
        else:
            ValueError("Unrecognized prompt template.")
            exit()

    return res
