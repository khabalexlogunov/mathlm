# MathLM

Solution for HWEI test task on adding knowledge of additive arithmetic to LLMs.

List of supported models:
- ```StarCoderBase-1b```
- ```Phi-1.5```
- ```Pythia-1b```

Each model has it's own basic pretrained and math finetuned versions.
Pretrained-only models could produce different unsystematic continuations - so pipeline may stuck on postprocessing errors during ```sum.py``` execution.

## Installation
```python
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Usage
Main inference logic incapsulated in ```sum.py``` script. So, to start process just call that script with parameters you need.

Command below runs pretrained ```starcoderbase-1b``` model (by default) on numbers 250.0 and 0.10000001: 

```python
python -m sum --term1=250.0 --term2=0.10000001

# Output:
# 250 + 0.10000001 = 251.0
```

As mentioned, each of listed models has it's own finetuned version, which could be used by calling ```--use-ft``` flag. Next snippet will run addition of two pairs using LoRA-adapters for previous LLM:
```python
python -m sum --term1=0.44,250.0 --term2=546.34,0.10000001 --use-ft

# Output:
# 0.44 + 546.34 = 546.78
# 250 + 0.10000001 = 250.10000001
``` 


All possible pipeline execution configurations are listed here:
```python
# pt and ft StarCoderBase-1b
python -m sum --term1=250.0 --term2=0.10000001 --system="starcoder-1b"
python -m sum --term1=250.0 --term2=0.10000001 --system="starcoder-1b" --use-ft

# pt and ft Phi-1.5
python -m sum --term1=250.0 --term2=0.10000001 --system="phi-1_5"
python -m sum --term1=250.0 --term2=0.10000001 --system="phi-1_5" --use-ft

# pt and ft Pythia-1b
python -m sum --term1=250.0 --term2=0.10000001 --system="pythia-1b"
python -m sum --term1=250.0 --term2=0.10000001 --system="pythia-1b" --use-ft
```